<?php

/**
 * Plugin Name: m4v Partido
 * Version: 1.0
 * Plugin URI: http://www.m4v.es/
 * Description: Este plugin nos permitirá mostrar los resultados de las ligas favoritas.
 * Author: Juan Manuel Gallejones
 * Author URI: http://www.m4v.es/
 * License: GPL v3
 */

/**
 * m4v Partido
 * Copyright (C) 2016, m4v - info@m4v.es
 */

/*----- Creación del post personalizado para Partidos -----*/

function partido_create_post_type() {

	$labels = array(
        'name'                  => _x( 'Partidos', 'post type general name'),
        'singular_name'         => _x( 'Partido', 'post type singular name'),
        'add_new'               => _x('Añadir Nuevo Partido', 'post'),
        'add_new_item'          => __('Añadir Nuevo Partido'),
        'edit_item'             => __('Editar Partido'),
        'new_item'              => __('Nuevo Partido'),
        'all_items'             => __('Todos los Partidos'),
        'view_item'             => __('Ver Partido'),
        'search_items'          => __('Buscar Partido'),
        'not_found'             => __('No se encontraron partidos'),
        'not_found_in_trash'    => __('No se encontraron partidos en la papelera'),
        'parent_item_colon'     => __(''),
        'menu_name'             => __('Partidos')
    );
    $args = array(
        'labels'                => $labels,
        'description'           => 'Post personalizado para Partidos',
        'has_archive'           => false,
        'public'                => false,
        'hierarchical'          => true,
        'show_ui'               => true,
        'supports'              => array( 'title', 'excerpt', 'thumbnail' ),
        'taxonomies'            => array( ),
        'exclude_from_search'   => false,
        'capability_type'       => 'post',
        'menu_position'         => 4,
        'rewrite'               => array( 'slug' => 'partidos' )
    );

    register_post_type( 'partido', $args );
}
add_action( 'init', 'partido_create_post_type' );

/*----- Añadimos la taxnomia para añadir los distintos deportes -----*/

function taxonomia_deportes() {
    
    $labels = array(
        'name'                  => _x( 'Deportes', 'taxonomy general name' ),
        'singular_name'         => _x( 'Deporte', 'taxonomy singular name' ),
        'search_items'          => __( 'Buscar Deportes' ),
        'all_items'             => __( 'Todas' ),
        'parent_item'           => __( 'Deporte padre' ),
        'parent_item_colon'     => __( 'Deporte padre:' ),
        'edit_item'             => __( 'Editar Deporte' ), 
        'update_item'           => __( 'Actualizar Deporte' ),
        'add_new_item'          => __( 'Añadir' ),
        'new_item_name'         => __( 'Nuevo Deporte' ),
        'menu_name'             => __( 'Deporte' )
    );
    $args = array(
        'labels'                => $labels,
        'hierarchical'          => true,
        'show_admin_column'     => true,
        'query_var'             => true,
        'rewrite'               => true
    );
    register_taxonomy( 'deporte', 'partido', $args );
}
add_action( 'init', 'taxonomia_deportes', 0 );

/*----- Registramos una taxonomia para las jornadas -----*/

function taxonomia_jornadas() {

    $labels = array(
        'name'                  => _x( 'Jornadas', 'taxonomy general name' ),
        'singular_name'         => _x( 'Jornada', 'taxonomy singular name' ),
        'search_items'          => __( 'Buscar Jornadas' ),
        'all_items'             => __( 'Todas' ),
        'parent_item'           => __( 'Jornada padre' ),
        'parent_item_colon'     => __( 'Jornada padre:' ),
        'edit_item'             => __( 'Editar Jornada' ), 
        'update_item'           => __( 'Actualizar Jornada' ),
        'add_new_item'          => __( 'Añadir' ),
        'new_item_name'         => __( 'Nueva Jornada' ),
        'menu_name'             => __( 'Jornada' )
    );
    $args = array(
        'labels'                => $labels,
        'hierarchical'          => true,
        'show_admin_column'     => true,
        'query_var'             => true,
        'rewrite'               => true
    );
    register_taxonomy( 'jornada', 'partido', $args );
}
add_action( 'init', 'taxonomia_jornadas', 1 );

/*----- Creamos el shortcode -----*/

function partidos_listado_shortcode($atts) {

    // generamos las variables, con las opciones por defecto
    extract( shortcode_atts( 
        array(
            'post_type'         => 'partido',
            'post_per_page'     => '-1',
            'orderby'           => 'date',
            'order'             => 'DESC',
            'deporte'           => '',
            'jornada'           => ''
        ), 
    $atts ));

    // asociamos las variables y generamos el query necesario
    $options =  array(
        'post_type'             => $post_type,
        'post_per_page'         => $post_per_page,
        'orderby'               => $orderby,
        'order'                 => $order,
        'deporte'               => $deporte,
        'jornada'               => $jornada
    );
    
    $query = new WP_Query($options);

    // pintamos los resultados de los partidos
    if ( $query->have_posts() ) {
    ?>
        <section class="scores-list">
    <?php 
        while ( $query->have_posts() ) : $query->the_post();
            // obtenemos el extracto del partido y su imagen destacada
            $excerpt_match  = get_the_excerpt();
            $url_match      = wp_get_attachment_thumb_url( $thumb_id );
    ?>
            <article <?php post_class(); ?>>
                <h2><?php the_title(); ?></h2>
                <?php 
                // comprobamos la presencia de imagen destacada
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( medium, $attr ); 
                }
                ?>
                <p><?php echo $excerpt_match; ?></p>
            </article>
    <?php
        endwhile;
        wp_reset_postdata();
    ?>
        </section>
    <?php
    } 
}
add_shortcode( 'listar-partidos', 'partidos_listado_shortcode' );

?>