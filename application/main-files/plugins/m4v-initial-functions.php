<?php

/**
 * Plugin Name: m4v Initial functions
 * Version: 1.0
 * Plugin URI: http://www.m4v.es/
 * Description: Con este plugin añadimos nuevas funcionalidades a la carga de wordpress. 
 * Author: Juan Manuel Gallejones
 * Author http://www.m4v.es/
 * License: GPL v3
 */

define('TEMPLATEURI', get_stylesheet_directory_uri());

//////////////////////////////////////////////////////////////////////////////////////////
// Cambiar la imagen de cabecera del login del site
add_action("login_head", "my_login_head");
function my_login_head() {
	echo "
	<style>
	body.login {
		background-color: #fff;
	}
	body.login #login h1 a {
		background: url('".TEMPLATEURI."/static/img/logo-m4v.png') no-repeat scroll center top transparent;
		height: 47px;
		width: 200px;
    	margin: 0 auto 20px;
    	outline: none;
	}
	</style>
	";
}

//////////////////////////////////////////////////////////////////////////////////////////
// Eliminamos welcome panel del dashboard
add_action( 'load-index.php', 'remove_welcome_panel' );

function remove_welcome_panel()
{
    remove_action('welcome_panel', 'wp_welcome_panel');
    $user_id = get_current_user_id();
    if (0 !== get_user_meta( $user_id, 'show_welcome_panel', true ) ) {
        update_user_meta( $user_id, 'show_welcome_panel', 0 );
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
// Widget para el Dashboard

function custom_dashboard_widget() {
    ?>
        <img src="<?php echo TEMPLATEURI; ?>/static/img/logo-m4v.png" />
        <h1 style="line-height: 1.2em">Bienvenidos al &Aacute;rea de Administraci&oacute;n de la web de Almanaque deportivo</h1>
<?php }

add_action( 'wp_dashboard_setup', 'my_dashboard_setup_function' );

function my_dashboard_setup_function() {
    add_meta_box( 'my_dashboard_widget', 'Bienvenido al administrador de la web', 'custom_dashboard_widget', 'dashboard', 'normal', 'high' );
    
}

//////////////////////////////////////////////////////////////////////////////////////////
// Eliminar icono de Wordpress de la barra de admin

function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);

//////////////////////////////////////////////////////////////////////////////////////////
// Que el logo no apunte a Wordpress.org
add_filter( 'login_headerurl', 'custom_login_header_url' );
function custom_login_header_url($url) {
    return 'https://www.m4v.es/';
}

//////////////////////////////////////////////////////////////////////////////////////////
// Cambiar el pie de pagina del panel de Administración
function change_footer_admin() {
    echo '&copy;'.date("Y").' m4v.es. Todos los derechos reservados';
}
add_filter('admin_footer_text', 'change_footer_admin');

?>
